/**
 * Created by MAX on 10/6/2560.
 */
angular.module("index", [
    'geolocation', 'ngMap', 'ui.bootstrap'
])
    .controller("IndexController", function ($scope, geolocation, Api, NgMap, Notification, $uibModal, $rootScope) {
        $scope.googleMapsUrl = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAmp7wfaKmrCr15Y9CUI1I9HuxH3kGrZd4";
        $scope.onLoad = true;
        $scope.coords = {
            lat: 18.796143,
            lng: 98.979263
        };
        $scope.radius = 500;
        $scope.keywordSearch = '';
        $scope.places = null;
        geolocation.getLocation().then(
            function (data) {
                if (data && data.coords && data.coords.latitude) {
                    $scope.coords.lat = data.coords.latitude;
                }
                if (data && data.coords && data.coords.longitude) {
                    $scope.coords.lng = data.coords.longitude;
                }
                $scope.callSearchPlace(true);
            },
            function (err) {
                console.warn(err);
                $scope.callSearchPlace(true);
            }
        );

        $scope.callSearchPlace = function (skipValidate) {
            if (!$scope.keywordSearch || $scope.keywordSearch.title === '') {
                var elem = angular.element('#search_value');
                if (elem) {
                    $scope.keywordSearch = {
                        title: elem.val()
                    };
                }
            }
            if (!skipValidate) {
                if (!$scope.keywordSearch || !$scope.keywordSearch.title || $scope.keywordSearch.title === '') {
                    return false;
                }
            }
            var result = Api.search_place({
                'lat': $scope.coords.lat,
                'lng': $scope.coords.lng,
                'radius': $scope.radius,
                'keyword': $scope.keywordSearch.title || ''
            });
            Notification.info({
                message: 'Loading...',
                delay: 60000,
                positionX: 'right',
                positionY: 'top',
            });
            result.then(
                function (res) {
                    if (res && res.data && res.data.data && res.data.success) {
                        Notification.clearAll();
                        $scope.places = res.data.data;
                    }
                },
                function (err) {
                    console.error(err);
                    Notification.clearAll();
                }
            );
            return result;
        };

        $scope.config = {
            autoHideScrollbar: true,
            theme: 'dark-3',
            advanced:{
                updateOnContentResize: true
            },
            scrollInertia: 0,
            axis: 'y'
        };

        $scope.stylesMap = [
            {
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.business",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.business",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.place_of_worship",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.place_of_worship",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "saturation": 50
                    },
                    {
                        "gamma": 0
                    },
                    {
                        "hue": "#50a5d1"
                    }
                ]
            },
            {
                "featureType": "administrative.neighborhood",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#333333"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "weight": 0.5
                    },
                    {
                        "color": "#333333"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "gamma": 1
                    },
                    {
                        "saturation": 50
                    }
                ]
            }
        ];

        $scope.objPlaceOver = null;
        $scope.placeOver = function (item) {
            $scope.objPlaceOver = item;
        };

        $scope.placeOverLeave = function () {
            $scope.objPlaceOver = null;
        };

        $scope.googleMap = null;
        NgMap.getMap('googleMap').then(function(map) {
            $scope.googleMap = map;
            if ($scope.googleMap
                && $scope.googleMap.shapes
                && $scope.googleMap.shapes.circle) {
                $scope.googleMap.shapes.circle.set('fillColor', '#66b2e3');
                $scope.googleMap.shapes.circle.set('strokeColor', '#66b2e3');
                $scope.googleMap.shapes.circle.set('strokeOpacity', '0.8');
                $scope.googleMap.shapes.circle.set('fillOpacity', '0.5');
                $scope.googleMap.shapes.circle.set('strokeWeight', '1.5');
                $scope.googleMap.shapes.circle.set('center', 'current-position');
                $scope.googleMap.shapes.circle.set('radius', $scope.radius);
                $scope.googleMap.shapes.circle.set('id', 'circle');
            }
        });

        $scope.detailModal = function (item) {
            if (!item || !item.place_id) {
                return false;
            }
            var scope = $rootScope.$new();
            scope.place_id = item.place_id;
            var settings = {
                animation: true,
                templateUrl: 'angular/home/modal.detail.html',
                controller: 'PlaceDetailModalController',
                size: 'lg',
                backdrop: 'static',
                scope: scope
            };
            return $uibModal.open(settings).result;
        };

    })
    .controller('PlaceDetailModalController', function ($scope, $uibModalInstance, Api, $timeout) {
        $scope.placeDetail = null;
        $scope.onload = true;
        Api.get_place($scope.place_id).then(
            function (res) {
                $timeout(function () {
                    $scope.onload = false;
                }, 1000);
                if (res && res.data && res.data.data && res.data.success) {
                    $scope.placeDetail = res.data.data;
                }
            },
            function (err) {
                $timeout(function () {
                    $scope.onload = false;
                }, 1000);
                console.error(err);
            }
        );


        $scope.close = function () {
            $uibModalInstance.close("close");
        }
    });