angular.module("api", ['ui-notification'])
    .config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {'X-Requested-With': 'XMLHttpRequest'};
        if (window.Laravel && window.Laravel.csrfToken) {
            $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
        }
    })
    .service("Api", function ($http) {
        var prefix = '/';
        return {
            post: function (url, data) {
                return $http({
                    method: "POST",
                    url: prefix + url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param(data)
                });
            },
            get: function (url) {
                return $http.get(prefix + url);
            },
            search_place: function (data) {
                return this.post('search_place', {
                    'search': data
                });
            },
            autocomplete_search_place: function (keyword) {
                return this.get('autocomplete_search_place?keyword='+keyword);
            },
            get_place: function (place_id) {
                return this.get('get_place?place_id='+place_id);
            },
        }
    });