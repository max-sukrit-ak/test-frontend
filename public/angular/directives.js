angular.module("directives", [])
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    if (scope.$root.$$phase != '$apply' && scope.$root.$$phase != '$digest') {
                        scope.$apply(function (){
                            scope.$eval(attrs.ngEnter);
                        });
                    }

                    event.preventDefault();
                }
            });
        };
    })
    .directive('onErrorSrc', function ($timeout) {
        return {
            link: function (scope, element, attrs) {
                $timeout(function () {
                    if (!attrs.ngSrc && attrs.ngSrc != attrs.onErrorSrc) {
                        attrs.$set('src', attrs.onErrorSrc);
                    }
                }, 100);

                element.bind('error', function () {

                    if (attrs.src != attrs.onErrorSrc) {
                        attrs.$set('src', attrs.onErrorSrc);
                    }
                });
            }
        }
    })
;