/**
 * Created by MAX on 10/6/2560.
 */
var app = angular.module('test_frontend', [
    'ui.router', 'routes', 'api',
    'ui-notification', 'angucomplete', 'directives', 'ngScrollbars',
    'index'
])
    .config(function(NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 10000,
            startTop: 70,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'bottom'
        });
    })
    .config(function (ScrollBarsProvider) {
        ScrollBarsProvider.defaults = {
            scrollButtons: {
                scrollAmount: 'auto', // scroll amount when button pressed
                enable: true // enable scrolling buttons by default
            },
            axis: 'yx' // enable 2 axis scrollbars by default
        };
    })
    .run(function ($rootScope) {

    });