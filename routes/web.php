<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::post('search_place', 'ApiController@search_place');
Route::get('autocomplete_search_place', 'ApiController@autocomplete_search_place');
Route::get('photo_place', 'ApiController@photo_place');
Route::get('get_place', 'ApiController@place_detail');

//Route::get('photo_place_1',
//    function () {
//        $d =  \GoogleMaps::load('placephoto')
//            ->setParamByKey('maxwidth', '400')
//            ->setParamByKey('photoreference', 'CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU')
//            ->get();
//        return \Intervention\Image\Facades\Image::make($d)->response();
//    });
