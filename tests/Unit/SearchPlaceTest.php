<?php
/**
 * Created by PhpStorm.
 * User: MAX
 * Date: 10/6/2560
 * Time: 18:54
 */

namespace Tests\Unit;


use App\Http\Library\BuildResultGooglePlace;
use App\Http\Library\BuildResultGooglePlaceAutoComplete;
use App\Http\Library\BuildResultGooglePlaceDetail;
use Tests\TestCase;

class SearchPlaceTest extends TestCase
{
    public function testSearchPlace()
    {
        $res = $this->json('POST', 'search_place', [
            'search' => [
                'lat' => 18.7717874,
                'lng' => 98.9742796,
                'radius' => '500',
                'keyword' => 'อาหาร'
            ],
        ]);
        $res->assertStatus(200)->assertJson([
            'success' => true,
        ]);
    }

    public function testLibSearchPlace()
    {
        $res = \GoogleMaps::load('nearbysearch')
            ->setParam([
                'location' => '18.7717874,98.9742796',
                'radius' => '500',
                'keyword' => 'food',
//                'language' => 'th',
//                'type' => 'food',
            ])
            ->get();
        $this->assertNotEmpty($res);
        $result = json_decode($res, true);
        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('results', $result);
        $this->assertNotEmpty($result['results']);

    }

    public function testBuildResultGooglePlace()
    {
        $str = '{
   "html_attributions" : [
      "Listings by \u003ca href=\"http://www.where-in-thailand.com/\"\u003eWhere In Thailand\u003c/a\u003e",
      "Listings by \u003ca href=\"http://www.openrice.com/\"\u003eOpenRice\u003c/a\u003e"
   ],
   "next_page_token" : "CpQCAwEAAD_qky4CP1o4y3l1p8yl8g0_ohSvwnp9x6IzW8IK4A8TOpOfZKFBI6pKrA4xNOJR3AC9E6P9uR0_M3TrGAtn-jm5nYVItrwZZoBc2GRK_2CRnMSgbk8diqEI3kHoiWxjHfYWPvV5IG8km00S-XTGiNgikYG3lsLapARFfkkOtSj51L3uTvXl7S8Z5p4al6lUPrn_pDwd2SjXPiaQClWacROryrprDeNWYdW0TbT5cahG4BjuDozmcekIWjyhFZnC7iGZTlNuj1_hyi8id6S3CsyxGkni0XwWZHACB_lwj7utx9ud_t7vc1XOK9lByFL9ZxEPUcCfS0bjpS-GWg80TwSkuqvJ0FJDzQpe2IdOge6XEhClPrqEfkSuK87lJOJltVo7GhTDEvwSkq-LFIlRgIQaQqGqCQVktA",
   "results" : [
      {
         "geometry" : {
            "location" : {
               "lat" : 18.769681,
               "lng" : 98.9753322
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7710299802915,
                  "lng" : 98.97668118029151
               },
               "southwest" : {
                  "lat" : 18.7683320197085,
                  "lng" : 98.9739832197085
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "8f315c3486ae282bf8950155de8a901945843ef5",
         "name" : "Sizzler",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 2988,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/103703449366743461280/photos\"\u003eSuper M Zza\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAMeifZabtN-MNDlnokLHIXg74vnGHGxEsJLBNlyWrsXfyH_dWexcQrees78PnKkD38yb6IpSLa4zpvuePu7Acvv4y-3TiFmAjeVrmJAvAg7a3aL-4F_U0fPV3mjKjTRGFEhDy5r5H_qgwhfYsLekCZYAlGhTSfFHpQhgQ1O3jKQIRCj60CfTn7Q",
               "width" : 5312
            }
         ],
         "place_id" : "ChIJebEWqGQw2jARO_8AF2JFO4o",
         "price_level" : 1,
         "rating" : 4.1,
         "reference" : "CmRSAAAAbmrPXBEgjynCJkpA6NaStuhU6FN91_hmFTmIkNIe-TLheaMjrRIrpd60zxSsnYwmPYYj-dIFQawZKoG84LmCIsFcKyHL-IwLS2StSMVDFDZT5wIBMVBgy_PgMRKCc7KMEhDLBUP6JxHSXsM3hQYnTEO8GhT2JekIEC4UOPcYtfDy8CzrFsMolQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "เซ็นทรัล แอร์พอร์ต พลาซ่าชั้น 4, ถนนมหิดล, อำเภอเมือง, เชียงใหม่, 50110"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7723766,
               "lng" : 98.97812909999999
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7736166802915,
                  "lng" : 98.97927523029151
               },
               "southwest" : {
                  "lat" : 18.7709187197085,
                  "lng" : 98.9765772697085
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "856886f4ac912086ed5552d7a2f4120f417de187",
         "name" : "Ohkajhu Organic Restaurant",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 2560,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/108185369210462987148/photos\"\u003eGary Collins\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAA5P08vfQkj0bf7AsO5CbCyQGU5omMwGPcJbZI1rA_k4sHtaeLsvZ3Olj-Ms8p4dgfLG-5pe2Lblc8vRNwsTbVaRGnippKg9C0RcXCr7hUox_DcT3s9twXP-aFCQ00DrOeEhD9i0B6c6V04iGSNN2_VT3EGhS-qMnjqFY6LVFlowbzVACjbRH_7w",
               "width" : 1440
            }
         ],
         "place_id" : "ChIJkbRO9How2jARc6yLSe4oyXI",
         "rating" : 4.5,
         "reference" : "CmRRAAAAeI-nwEpznbAXgu-8Kaw10mCjuhDH1STCLmnuht2XW0qIXO_HObhjz8etWeqf_Umgg0cpRphsfQS_x52VRb7lsZ2Lc825OgnIjCJT5ca_SYcHwWoTNHWQ2RCT---S05pyEhDRLMzqP52CQovWm0AKZCiaGhRiNL7TEFy6tnYZYTUC9bXhHVsSSA",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "17 ซอย 1, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7694678,
               "lng" : 98.97547759999999
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7708167802915,
                  "lng" : 98.9768265802915
               },
               "southwest" : {
                  "lat" : 18.76811881970849,
                  "lng" : 98.97412861970848
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "319ba61e120ef25f39502e9a3ce08b268e4fc155",
         "name" : "Secret Recipe",
         "place_id" : "ChIJPVt1tWQw2jARO4KcZ6UylEM",
         "reference" : "CmRRAAAAjpNiPjZEb6eWggv301mxZMqJjui5njUGxQILoWNknLtoeFN-RdZGXf9OkKWiBsgldmf_EiX3gjzThEohWT_ZlBoQydwg8oBRNhIn89frFlG95rhxNImleqFbJCDs3uSwEhDF1jan2Q-9fIVcEO2X16GWGhRLsPfLh2Y4015Pto0UYCvDcAIw7Q",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "Mahidol 2 Alley, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7702523,
               "lng" : 98.9745904
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7716012802915,
                  "lng" : 98.97593938029151
               },
               "southwest" : {
                  "lat" : 18.7689033197085,
                  "lng" : 98.9732414197085
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "3b08ce2434168027abd8b5fb1bf3116a309df6c6",
         "name" : "Suan Paak",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 3096,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/105513505823738876000/photos\"\u003epasso chan\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAA_Mew1G8-nWJCPJutQ3o4Wr_4CEFQh9A6NJcndDxzxVfqrvFDJ5WlOnjiPS_kH7c1vYEm1Q1MF0H6ymCp35-oxZ6TNgtLz4s_0rbMo5l4MMrBBGRcAfUUvE_vaEAHDyM7EhAfHz4CsRT1YT41VQJ8Yfw8GhTq_1LarsEZOynRG5NkLmvfzny5Dw",
               "width" : 4128
            }
         ],
         "place_id" : "ChIJ60DBVnsw2jAR9ZKPIny2HMI",
         "rating" : 4.1,
         "reference" : "CmRSAAAA8v84y208n7MO7PwPhktrcMBdoGkW0cfuUVHMZwUYnxkwDXDFYgKn0DYbklrLfpEC4GzO6oEvrEWwJi8pkZ6KDc-dTzbjytZc8qoJE6SZ1wb18fZzH_T-5_CUB-5WWje6EhBE9yJqkUfm_HB9OJbjfbZRGhQxFPYAsbVuALtZ1BELb9NE-eMplA",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "Chiang Mai, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7691672,
               "lng" : 98.9757942
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7705161802915,
                  "lng" : 98.97714318029151
               },
               "southwest" : {
                  "lat" : 18.7678182197085,
                  "lng" : 98.97444521970849
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "d81b4b1f203452436fc4c8fcd3636a6204ac6cea",
         "name" : "Red Mango",
         "place_id" : "ChIJdRdeyGQw2jARqvvVa2sq6M8",
         "reference" : "CmRSAAAApsHe6Ffkxkjn9KRWWPJbOr7Ln7CtVrPIM1PvHus-S0bqisGUdx94leNboTdI12nrsl9KJ08dghpqydpKV4JOr_slbWdOT9E3BqBmvRXyQAu85gAH1lNXo5LSwuEKZHCnEhB1unM1S3CaikVL40LL6DlDGhRxLdsYgasfxz1oGzJR7lufKy1iyg",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "Mahidol 2 Alley, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7724013,
               "lng" : 98.97829229999999
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7737502802915,
                  "lng" : 98.9796412802915
               },
               "southwest" : {
                  "lat" : 18.7710523197085,
                  "lng" : 98.9769433197085
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "0cce0bbdf0a32e84b03c6fc598d93bf27fb3c09b",
         "name" : "Oh ka Jhu",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 3264,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/111491475583803337878/photos\"\u003eChaisiri Toomthong\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAEJbJwPAQxYkytFfwo1EuAcWpyWCBYZ3pnXPd1hOugLDfFmJocEX7DyZV4e6ED44F-vtyQs7slD8AR5BLF8we02uHbmtNxyNXymHFOHzullBa8ODGTym-3w-NAbw1q_EBEhDAkEoAfhGglSPrv-1IZ6HXGhQJNpP9UPyPyfuDhKaqPt8cRfWvCA",
               "width" : 4912
            }
         ],
         "place_id" : "ChIJDRco93ow2jARt5rR9Cwq4t0",
         "rating" : 4.4,
         "reference" : "CmRSAAAAq6BJZvZNPm7jreYNI2h6Uv9u-0pLf7lH2iZhDo5i47gqr80ooyTgT4-aL2JT2DHqtjf-yVVsSFFN9q5QC5n_5Y_NU2t_0eCZ-AzrW1Xmwc3ahNjb02ObONrptG5dHtWTEhCFH85GS4WGJYiizvRI9bCvGhSK2BvZ7WLVMwint30ekOTysSBSLA",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "18°46\'20. 98°58\'40., 2"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7713246,
               "lng" : 98.97984029999999
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7726735802915,
                  "lng" : 98.9811892802915
               },
               "southwest" : {
                  "lat" : 18.7699756197085,
                  "lng" : 98.9784913197085
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "67a70995a86bbbd1f2eef59d5c86f7578a855705",
         "name" : "Mama\'s Kitchen",
         "opening_hours" : {
            "open_now" : true,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 3024,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/103846810156527121276/photos\"\u003eVictoria Lesce\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAhGDZdNRe28z5Lqs8KYo_ve-Foplb4yfQnO73R6euUq_kNcuyFVRNuXMR54QGm5WFGbw8JjTMUSWmg0-AukfbM2Y493fXPnCihZZy7Zmu8153bGPLNn4ybrBlklLJIwlDEhBncgUC4k6LgknuW3iPhWV7GhTP62No_36kH1L3WEXnjRma6BX3rw",
               "width" : 4032
            }
         ],
         "place_id" : "ChIJf7rBdXAw2jARyqQDnUTzYjs",
         "rating" : 5,
         "reference" : "CmRRAAAAXOd3_NN3Wp5z7YGokE53P7WhmIlEwsMskIN-sGbP48WyNdnHPu7S6z2HI0kJp-0VZPhBvCEi9HLVhFr3wsL57jzkq-8rAe_9NDYT0YWdmUTpRF6VO7WB1X022i8TgfArEhDHOIXOMX6OmT3b07tIqWs4GhSqGeUshpcVhftDeMzgSNRgkYSBfQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "Hai Ya, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7687611,
               "lng" : 98.9747479
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7701100802915,
                  "lng" : 98.97609688029151
               },
               "southwest" : {
                  "lat" : 18.7674121197085,
                  "lng" : 98.97339891970849
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "b1972203642d4d217c74f9a7c0d1bcff1aa1078b",
         "name" : "Oishi Buffet",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 960,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/110972741977303012516/photos\"\u003eชาย ต้อม\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAp_h8WMdgPA0k7ApN6wH53Xdx1iptGAxUqcZFEUIIAx2cQqYlLeFIbWzT7vyqaha47PrQ6XrBB0zS7aGt5hu3eZYU74ZZVL03dxLmy074eJ6AYc0w_KoUlvx_Gw0seRp4EhCFmBt4Z3Utw7BcjGd65cyzGhTN85cGX1iqOXFh6bJbaaLkcwdrIw",
               "width" : 1280
            }
         ],
         "place_id" : "ChIJV9ZzzGQw2jAR2jptqstAux4",
         "rating" : 4,
         "reference" : "CmRRAAAAQAD9Xuafg54RPh7dST5NkQSJXbsAKBTFvfIO9ohiuzIqst0Um9JAT_1gTW7yZrrrygNoEMNnLRvKbRB7bJPaWS5f8e_edwqe51rm0LZ37FMTL3htnV05bQdFBY4O4aj3EhDQfaugjGwwi7ecCcn1Zyn4GhSXkbif_fVHaGSvja7nPZU6VYR-qQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "2, Central Plaza Chiangmai Airport, 4th Floor, Room 454-456, Mahidon Road, Tambon Haiya, Amphoe Mueang Chiang Mai, Chiang Mai, 50000"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7715163,
               "lng" : 98.9736291
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7728652802915,
                  "lng" : 98.97497808029151
               },
               "southwest" : {
                  "lat" : 18.7701673197085,
                  "lng" : 98.97228011970849
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "c1f4c66382e2ba432acd4071b2c9a329e5e5f111",
         "name" : "ฟองดูว์เฮ้าส์",
         "photos" : [
            {
               "height" : 2988,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/110703136454359750829/photos\"\u003eXavier Arte\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAwQaW-ikJ4fWLvhJ8KpPduyQz21nHEmgQwn2mwNJrDavhFWrdS9kKj0g77AUB2jUMX1oq07lAd2eT0ZdjI2FDwLJ5_xHKRnbfvwZYhrUdmOMqlp2JPHYLL9XLtjcTHaAjEhDYQXnS6qbrFkfb0XpqKPSBGhTacR9c6zPKVq477om-fmwtqwzdiw",
               "width" : 5312
            }
         ],
         "place_id" : "ChIJedRKmHww2jARDJQelZxhao0",
         "rating" : 4,
         "reference" : "CmRSAAAATNQ6gaHSJodY3ycV2MWgrWzxjgRsvHDBu2kcXLaarSyFczDZ_lGzPpj4SqkqUWyZStkXOAwWl8pYCyjY_xoJNfCFW73W411Iw_yJpKn1fYJjWX5o9FxqpoKWyLWxCwicEhA-evHTEHyuhG7yeUG8RDwBGhQDTrAsmXS_g6m-qb5Vxl2TeJdmUw",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "ตำบล สุเทพ, 18/1 Sanambin Rd. 50200, อำเภอเมืองเชียงใหม่"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7714642,
               "lng" : 98.97668969999999
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7728131802915,
                  "lng" : 98.9780386802915
               },
               "southwest" : {
                  "lat" : 18.7701152197085,
                  "lng" : 98.9753407197085
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "a3a5c7a121fb9a658faa3039d5f7d99d2a791961",
         "name" : "Buddy\'s terrace bistro",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 1080,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/103827236688545539943/photos\"\u003eGett Sutprattana\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAJIDB5ZqvQjTtGDSLgIXsITvIial1SRb2vtTaT32XP0IOY4nQRwEp98MtAO46wKxLSqu4Aygz0iDr5qizjmt0FRx6qVRU37ZbMqFXiqugqHy2d8njjCmPPziVn5DzloJqEhAkaFOjOqnwOBQugRiuPbaTGhRDx1-8DTvvOfaJPIA3_uV3SAegxA",
               "width" : 1920
            }
         ],
         "place_id" : "ChIJJXK4Hnsw2jARsg8Rlmxx3Uc",
         "rating" : 4,
         "reference" : "CmRRAAAAWe3V1UDyDBeO7q__p_PwMuVL72nZpIqiBbs2Vpo113F7jPDfQL2uY_EP8QhvLfKlwOJuOqIYDH78LhxqyeUEuI9IFFZ0LJiM5PbqITfFPuQ9HUGKmGYLYm628tnrMjHyEhBw51tt2qAzFuPKOJ1pWPzhGhT6nBFp8rbSh-ANhvzuOSFMLGWukQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "Hai Ya, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7745847,
               "lng" : 98.9770099
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7759596802915,
                  "lng" : 98.9781174302915
               },
               "southwest" : {
                  "lat" : 18.7732617197085,
                  "lng" : 98.9754194697085
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "4d01d9c3252639b3b0f12e70b2cb3d7652002b34",
         "name" : "BEEF LAND STEAKHOUSE",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 2610,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/102891057717129664165/photos\"\u003eAlexis Sirkia\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAA_EgZjdl8lQi8wm9fNzYB_omfP3KoSyUWH86z6k0S1MnDrm4qlQsWtwPk1wdimUEyHnGQaow7RDeYIVyvrdwHT7P4Af6UoqIdNam0-sAK9IXMH8MDXMLaY4lIdb8JRQA6EhB_2o4x_v8OzcvFuqH3rrNhGhSfA-Y3SCxpGqCKEUYJpI06fOfd_w",
               "width" : 4640
            }
         ],
         "place_id" : "ChIJg2I2M3ow2jARvNl4iar3VGY",
         "rating" : 5,
         "reference" : "CmRRAAAAjZvC7BtQXoJmcWl0kTlVh38Lf8BAB3Kuj7c7CHDx-u0qteSP9EKfOTmgOqKh_dN18ax8mo_PZ4QRWRdNRY7tTfAgv5q9U39SmtbCrtPVK-g1Fy9cFFxKkPG1ncwO65lBEhBirqdr0aJUI2EipjMvOlbVGhSPRD_B2ZC_9dYO629CeEHmMHFlZQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "107 Mahidol Road, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7694693,
               "lng" : 98.9751115
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7708182802915,
                  "lng" : 98.97646048029151
               },
               "southwest" : {
                  "lat" : 18.7681203197085,
                  "lng" : 98.97376251970849
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "32b7cafcc89b56144c261b4f258cade1a3177f5c",
         "name" : "Fuji",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 2448,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/116266927420214324948/photos\"\u003eFumihiko Fujimoto\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAULLO5vBepZv9k_4irIJxuCoQmlRj75YWvb51y__j7iWmtTExMFp3BRSpbz8om56Vq8M5GOQhyXdEb_eQuFV5Pepy_Kgcnf3qvdLF8HX9kccqdongFZCZCb6vxa6OnEWxEhB6IuRxzAouaNa4yy-TeQ2RGhQfMIlCOjvJ__eB4DHyf_VmApB5fQ",
               "width" : 3264
            }
         ],
         "place_id" : "ChIJZ1BPymQw2jAR-RPlPjK5-Fc",
         "rating" : 4,
         "reference" : "CmRRAAAAVJQMDMfEXaRvV-W3qhGwQ-z29d6q3mir_SUt8tCCQuq8zT4j4RrQOQ8lMZe8MW9sCQqBsAy49TqHqX-8a7CFSITAN0v1wzEs0eyQrDWb-E65HEVOn0Sy0AxzTR3cwut_EhCJ6mA8ofkghFQulqqt0DrSGhSB-gYPkJaaSxn7zQ0kvncvRPmjZA",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "2, Central Pattana Tower, Room 463-5, Mahidol Road, Tambon Pa Daet, Amphoe Muang, Chiang Mai, 50100"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.769349,
               "lng" : 98.975888
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7706979802915,
                  "lng" : 98.97723698029151
               },
               "southwest" : {
                  "lat" : 18.7680000197085,
                  "lng" : 98.9745390197085
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "b1672c4ba838c7eac8b84681fed96198e13e749b",
         "name" : "ภัตตาคารอาหารญี่ปุ่นฟูจิ",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 1365,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/101284213540192712171/photos\"\u003eภัตตาคารอาหารญี่ปุ่นฟูจิ\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAUqOHIT9FatOl2TVKofkkjGRoPpB43Dwt8Xs-lRUIkHjLxYhWK6IKRsB2zHtwEF_v-FlI7LJUFNZjDd_tge3Xuqdxv-oKJr2yF0OVUbuPimR3dNl92neFnCbDLmLmlFvGEhCs8XFQEelgFoUpbCGTvMJ6GhQmVfjHOni9Owrq6i4ZGNvKJb2UaQ",
               "width" : 2048
            }
         ],
         "place_id" : "ChIJxXBbyWQw2jARBOEbvEKLSWA",
         "rating" : 5,
         "reference" : "CmRRAAAA99lvSOyIDqGIn3yJ_jc_y75XadyUobIZH55HYtHChRYwQ7KH0VSiMTbRAu8tuYXAkvbWyxDXQxZ4A5AdwzyrGBhPW49oOwpyUHglvIatbKyXD-fURDgnpZyTsKlAerqCEhDqH__Vm-s7UScnZf_LBeugGhTGU92DEqEhMEIuCFwxNZWUX5J8RQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "2 อาคารเซ็นทรัลพัฒนา ห้องเลขที่ 463-5, ถนนมหิดล, หายยา, เมืองเชียงใหม่"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.770701,
               "lng" : 98.97660999999999
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7720499802915,
                  "lng" : 98.9779589802915
               },
               "southwest" : {
                  "lat" : 18.7693520197085,
                  "lng" : 98.97526101970848
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "a38e9cbbf15de295a80e0260b07edbc13a7f84bf",
         "name" : "Grill Jung 2",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 2660,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/115631192747203588216/photos\"\u003eNimnual Thummarin\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAABjMoxrWKmgf1S8zzM3ZYKTyiSCO5yrEuf4kP-mNTtBiQwuIH1FAXJGlhkgpyv9AvbSPe_deVZwWpvrTheXTZ780EbYMf3NQYPETiWVH-6Wc8dL5eaNPiM1rMTVkXUtMAEhB9EDBZVYV1POCn_hfr4y2dGhRyKFJT8HsM8w-D-lwQ603zfdc4nA",
               "width" : 4730
            }
         ],
         "place_id" : "ChIJXV0kJHsw2jARDwDncJqvYNQ",
         "rating" : 4.4,
         "reference" : "CmRSAAAAS7ao9wlldYd2zl0wVaitPoMZgYyNL8q-ZDX7YYQwwnfiqHu8yMwexHjAwbKM1xwLJXv0xnF1GnXbD7CfV-CqHSMzmweR_xqWhRkVMo52b2oDua5iaCgre7S7TQjLwPZCEhAmOAGdyy8Apu8U_rsPl4PJGhR6Rf2Yuuzwzli-_IEt7_Q5xO8wBQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "Soi Mahidon 1, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7724823,
               "lng" : 98.98001200000002
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7738312802915,
                  "lng" : 98.98136098029151
               },
               "southwest" : {
                  "lat" : 18.7711333197085,
                  "lng" : 98.97866301970851
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "15e0fd87ae43b9f3c36581386bd37ce4cf1533b0",
         "name" : "Hanawa Japanese Restaurant",
         "opening_hours" : {
            "open_now" : true,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 2048,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/116281290893322023577/photos\"\u003ebentou06\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAXAREqJumNVxRwHB6WxQ6Df8kqwdQy2yUxy9FIBexM6jEx9mML8qdG4HhVwWCsdSREMjlh0M6Wng07rR5X9AT8Dixxc60vCaKoZfYmdNaHXF1UniPy6KJWShMB3PT4zDgEhANzinEUg8tPUmCR4tywcseGhR1Ei1K-CtbSXwBAeoGbxEJamRgeQ",
               "width" : 1536
            }
         ],
         "place_id" : "ChIJyxMffnAw2jAROAcXV-TKFXg",
         "rating" : 4.7,
         "reference" : "CmRRAAAA9H4sZQQm3f4nRkJltthmhhsdQgb0uXs_Ji-oXoDcP2tdduKbRseBY9Lrwsl8VDwNx2u7l0a8KG9kldSeZs6iDVzVOEk7Wc7eJLZRDLtFsID4KojCvj_0srTlwIGd0h-XEhCTB0OUdMzRYGo6D5EqBEHKGhSvgTDwHf4kMGGyug32tr-qF6IuxQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "Hai Ya, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7694395,
               "lng" : 98.9752376
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7707884802915,
                  "lng" : 98.9765865802915
               },
               "southwest" : {
                  "lat" : 18.7680905197085,
                  "lng" : 98.97388861970849
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "62d845a9b5217d73b419cbbc8d1437526e98f9ee",
         "name" : "Santa Fe Steak House",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 3120,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/117718159421750131400/photos\"\u003eTaweesin Ritt\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAtCGvuDUksbHkRj1yutJc5Cd8BiX57_olRQvPmGTCx_driiBuEbuUvPwb33uyV0Ocg58hgu5scES-Qh6InZFpekvXgBDminXPrcFmhXLNbi4kDP2uwCKnTafAJsPiQaHDEhBGVPPKHrdePuMtZjEo6FOTGhRJQySIhFCbBa-0PX7capP_xlE7oQ",
               "width" : 4160
            }
         ],
         "place_id" : "ChIJvSL8s2Qw2jARMuzJiJsBgGs",
         "rating" : 2.8,
         "reference" : "CmRRAAAAYqz5DZIB-FALxV2ydpVtOdu_-4LsqqEqJSJAEpz4igdhpHGXOnn_zojtu8DHkxoLQP_mP5_d-WpnoZQaOJ3oHho_1wbJeP1GMv2qqBTQF6BR1QaxKCLUKPCyW0s0w2XvEhCxDtOxWJjXkIzXWwieyrkMGhQsq0YioaxG7teQMDeTgE7lIxB6gg",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7690514,
               "lng" : 98.9744125
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7707993802915,
                  "lng" : 98.97662290000001
               },
               "southwest" : {
                  "lat" : 18.7681014197085,
                  "lng" : 98.97367570000002
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "79aac0b573aad543bfeabcc29ecc0e176196b055",
         "name" : "Kad Luang Airport",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 2797,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/110201527108503376692/photos\"\u003eTanadon Chaobankoh\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAA-b9O-9fOUHaK1NItPtZp-AEvQlpcXUJPb5pjR02FgC0kzYyaAc11dhIgyb_r_s4CBuHgjDDU0QVzq0gKpdopXI-lkmpZbfnu91flqgjQ22yjsXgIFXZUQgGr_drK6tQUEhBBirjWJc0zFor0P1_8dOljGhT5Gwizv9XtSr6kBT0X0AxVCq64-w",
               "width" : 2797
            }
         ],
         "place_id" : "ChIJf9TVp2Qw2jARM_esC7q7xaA",
         "rating" : 4.5,
         "reference" : "CmRSAAAASKticpwQ73yVvcxB1RfrU0s2cjxhUT9ZDiRKADp7ZWVrK-c1w45xa7Kmdh0cOIUlgCAmhOKr4ohtb_faXqxqhMtjVfPWXmx-kfSYpuokZAvKf9eRnut7EDiR5W_x83gnEhBiX6-5kwJ9kPrWVnc0XCXqGhQA-QRri9RDUX7OpHKwmOC1qs16IQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "205 Mahidol Road, Pa Daet, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7724166,
               "lng" : 98.97835230000001
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7740650302915,
                  "lng" : 98.97949038029151
               },
               "southwest" : {
                  "lat" : 18.7713670697085,
                  "lng" : 98.9767924197085
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "71f655fef749c7ac723a05aa213c6d73512cff16",
         "name" : "be organic",
         "place_id" : "ChIJ8Xcs9Xow2jAR4Y9tJb2tswc",
         "reference" : "CmRRAAAAB6G58U6HWlo6QPZ3pgv2CU8L6Xikoy6DQUGYeiy7ArccmxoZJmK6rW-bCqYLJUhzMgduNrhPwCiJ_dCh7rnqg73gxBlwzy2nIZzuSYd-KrWGWBIiZxxPa0lg7ULX7RXfEhDZMe9mCcW56AYMuJewGD1nGhR9Sjenzbv9dVlMq9iDvZC9UY4qbg",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "17 ซอย 1, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.7750331,
               "lng" : 98.9786254
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7763820802915,
                  "lng" : 98.97997438029151
               },
               "southwest" : {
                  "lat" : 18.77368411970849,
                  "lng" : 98.97727641970849
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "29bfbdf2e86985fa7c40f62fb58c141c2a863b03",
         "name" : "MK Restaurant",
         "place_id" : "ChIJmTeVfXkw2jARMYyTBROx_a8",
         "rating" : 5,
         "reference" : "CmRSAAAAK7yxPOg580RVZT4QXKp4E9s33UZFkFEPpdQdofmkrorUGopM5IZUuPqbhc3OKztAQzJogVdpwC_bPS1nXgL6UuG7vx3NuHpjYh8T74_tfakBmGnv_bLLRBZanf3nThfLEhDbDygyEMuTTAfDpUW-vV4pGhSmLWztibK1h0vvyibhYO1WhJisiw",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
         "vicinity" : "โรบินสันเชียงใหม่แอร์พอร์ต Mahidol Road, Mueang Chiang Mai District"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : 18.769896,
               "lng" : 98.973941
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 18.7712449802915,
                  "lng" : 98.97528998029151
               },
               "southwest" : {
                  "lat" : 18.7685470197085,
                  "lng" : 98.9725920197085
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
         "id" : "91c282e2266f1afec664157dc547a2cc46ab6666",
         "name" : "VC@Suanpaak Boutique Hotel",
         "photos" : [
            {
               "height" : 600,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/114370465563852497480/photos\"\u003eวีซี แอ๊ด สวนผัก โฮเต็ล\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAmOLIu4_Hvwmq_PEAeGxeuye9QM5PMqgF7i16AYWsc1BbBLFdf7zc0wvtSYzlakMyO-5yccnBuDp2MkX7txTsZ-eAwTEP_VoW_LiX2xlIJv-F1SkP4mxWz6fy8QhCX_1MEhCV3Q-7vgoc171MMh4RQkCDGhQ4QnhneuA1HG_dc9cPueuhZkizRw",
               "width" : 800
            }
         ],
         "place_id" : "ChIJF-VFq3ww2jARlJFb0DHplTA",
         "rating" : 4.8,
         "reference" : "CmRRAAAAZT8V9TgIkWkXN5O77H6F5qSjqMTNT8ogDVuSY2JmP-z2dqbrNM-nqj0LvPNTEE6FzB0G9GsJ-kR9JI9rb4aopk_SaEgNA7lUfKxz2u1JFm31GjJPnUiNoKMlUnrUHYrGEhAbUY8muwYqxIZamAuaS9E0GhSxSHPAFbPjCCHZF4hTTVeCf_peRA",
         "scope" : "GOOGLE",
         "types" : [
            "general_contractor",
            "lodging",
            "restaurant",
            "food",
            "point_of_interest",
            "establishment"
         ],
         "vicinity" : "61 M.3 Airport Road, T.Suthep, A.Muang, Chiangmai Chiangmai"
      }
   ],
   "status" : "OK"
}';

        //https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&maxheight=300&key=AIzaSyAmp7wfaKmrCr15Y9CUI1I9HuxH3kGrZd4&photoreference=CmRaAAAAMeifZabtN-MNDlnokLHIXg74vnGHGxEsJLBNlyWrsXfyH_dWexcQrees78PnKkD38yb6IpSLa4zpvuePu7Acvv4y-3TiFmAjeVrmJAvAg7a3aL-4F_U0fPV3mjKjTRGFEhDy5r5H_qgwhfYsLekCZYAlGhTSfFHpQhgQ1O3jKQIRCj60CfTn7Q
        $builder = new BuildResultGooglePlace($str);
        $obj = $builder->get();
        $this->assertNotEmpty($obj);
        $this->assertEquals(20, $builder->count());
        $this->assertArrayHasKey('id', $obj[0]);
        $this->assertArrayHasKey('place_id', $obj[0]);
        $this->assertArrayHasKey('name', $obj[0]);
        $this->assertArrayHasKey('vicinity', $obj[0]);
        $this->assertArrayHasKey('types', $obj[0]);
        $this->assertArrayHasKey('rating', $obj[0]);
        $this->assertArrayHasKey('url_image', $obj[0]);
        $this->assertArrayHasKey('open_now', $obj[0]);
        $this->assertArrayHasKey('lat', $obj[0]);
        $this->assertArrayHasKey('lng', $obj[0]);
    }

    public function testLibSearchPlaceAutoComplete()
    {
        $res = \GoogleMaps::load('nearbysearch')
            ->setParam([
                'location' => '18.7717874,98.9742796',
                'radius' => '500',
                'keyword' => 'food',
//                'language' => 'th',
//                'type' => 'food',
            ])
            ->get();
        $this->assertNotEmpty($res);
        $result = json_decode($res);
        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('results', $result);
        $this->assertNotEmpty($result['results']);

    }

    public function xtestGetPlacePhoto()
    {

        $d = \GoogleMaps::load('placephoto')
            ->setParamByKey('maxwidth', '400')
            ->setParamByKey('photoreference', 'CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU')
            ->get();

        dd($d);
    }

    public function testGetPlaceQueryAutoComplete()
    {

        $res = \GoogleMaps::load('placequeryautocomplete')
            ->setParamByKey('input', 'Pizza near Lon')
            ->setParamByKey('language', 'th')
            ->get();
        $this->assertNotEmpty($res);
        $result = json_decode($res, true);
        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('predictions', $result);
        $this->assertNotEmpty($result['predictions']);
    }


    public function testGetObjectPlaceQueryAutoComplete()
    {

        $str = '{
   "predictions" : [
      {
         "description" : "Pizza, Bethnal Green Road, Londres, Reino Unido",
         "id" : "1e45f25d88ba70e0cc0b10f1b30be45b3cc8d418",
         "matched_substrings" : [
            {
               "length" : 5,
               "offset" : 0
            },
            {
               "length" : 3,
               "offset" : 27
            }
         ],
         "place_id" : "ChIJz1owz7AcdkgRHX3hlvjWBtI",
         "reference" : "CkQ-AAAAElLxGUoo_Nhr2ylu9EicX2fyxxxC8GgzywtdRUjIX1iNfGyf-GpbuwoNV9_O901rnEh75_0MpusaQM83LtbcUBIQC3-4xUpPY0Z2Z5HZgd-SIxoUXQ1Wk-QVFJ7babRjqNMJkDANqVA",
         "structured_formatting" : {
            "main_text" : "Pizza",
            "main_text_matched_substrings" : [
               {
                  "length" : 5,
                  "offset" : 0
               }
            ],
            "secondary_text" : "Bethnal Green Road, Londres, Reino Unido",
            "secondary_text_matched_substrings" : [
               {
                  "length" : 3,
                  "offset" : 20
               }
            ]
         },
         "terms" : [
            {
               "offset" : 0,
               "value" : "Pizza"
            },
            {
               "offset" : 7,
               "value" : "Bethnal Green Road"
            },
            {
               "offset" : 27,
               "value" : "Londres"
            },
            {
               "offset" : 36,
               "value" : "Reino Unido"
            }
         ],
         "types" : [ "establishment" ]
      },
      {
         "description" : "pizza ใน London, United Kingdom",
         "matched_substrings" : [
            {
               "length" : 5,
               "offset" : 0
            },
            {
               "length" : 3,
               "offset" : 9
            }
         ],
         "structured_formatting" : {
            "main_text" : "pizza",
            "main_text_matched_substrings" : [
               {
                  "length" : 5,
                  "offset" : 0
               }
            ],
            "secondary_text" : "ใน London, United Kingdom",
            "secondary_text_matched_substrings" : [
               {
                  "length" : 3,
                  "offset" : 3
               }
            ]
         },
         "terms" : [
            {
               "offset" : 0,
               "value" : "pizza"
            },
            {
               "offset" : 6,
               "value" : "ใน"
            },
            {
               "offset" : 9,
               "value" : "London"
            },
            {
               "offset" : 17,
               "value" : "United Kingdom"
            }
         ]
      },
      {
         "description" : "pizza ใน Long Beach, CA, United States",
         "matched_substrings" : [
            {
               "length" : 5,
               "offset" : 0
            },
            {
               "length" : 3,
               "offset" : 9
            }
         ],
         "structured_formatting" : {
            "main_text" : "pizza",
            "main_text_matched_substrings" : [
               {
                  "length" : 5,
                  "offset" : 0
               }
            ],
            "secondary_text" : "ใน Long Beach, CA, United States",
            "secondary_text_matched_substrings" : [
               {
                  "length" : 3,
                  "offset" : 3
               }
            ]
         },
         "terms" : [
            {
               "offset" : 0,
               "value" : "pizza"
            },
            {
               "offset" : 6,
               "value" : "ใน"
            },
            {
               "offset" : 9,
               "value" : "Long Beach"
            },
            {
               "offset" : 21,
               "value" : "CA"
            },
            {
               "offset" : 25,
               "value" : "United States"
            }
         ]
      },
      {
         "description" : "pizza ใน London, ON, Canada",
         "matched_substrings" : [
            {
               "length" : 5,
               "offset" : 0
            },
            {
               "length" : 3,
               "offset" : 9
            }
         ],
         "structured_formatting" : {
            "main_text" : "pizza",
            "main_text_matched_substrings" : [
               {
                  "length" : 5,
                  "offset" : 0
               }
            ],
            "secondary_text" : "ใน London, ON, Canada",
            "secondary_text_matched_substrings" : [
               {
                  "length" : 3,
                  "offset" : 3
               }
            ]
         },
         "terms" : [
            {
               "offset" : 0,
               "value" : "pizza"
            },
            {
               "offset" : 6,
               "value" : "ใน"
            },
            {
               "offset" : 9,
               "value" : "London"
            },
            {
               "offset" : 17,
               "value" : "ON"
            },
            {
               "offset" : 21,
               "value" : "Canada"
            }
         ]
      },
      {
         "description" : "Anna Maria Pizza, Long Island, Bedford Avenue, Brooklyn, NY, United States",
         "id" : "84481ec1a56e4400473c5ae98d445b3ec186f59a",
         "matched_substrings" : [
            {
               "length" : 5,
               "offset" : 11
            },
            {
               "length" : 3,
               "offset" : 18
            }
         ],
         "place_id" : "ChIJuw9Vxl1ZwokRhSvW0d_JRdA",
         "reference" : "CmRZAAAA3ghWZmwvYWNYrDZuc30NCLbe6Pf2q96yPZvpkiaSK268IcsfdTytVJUpvUFgaCmqKLYEwI50NAYJSNGI8_JefGR3VacjvJYqPMlHayxVFJXNRiLxl0WS3Y1wD0lTv5SIEhBXa0c813OfytyaiZ03UuhhGhSug03LhkecmW-K4fENR4zfJFD2FQ",
         "structured_formatting" : {
            "main_text" : "Anna Maria Pizza",
            "main_text_matched_substrings" : [
               {
                  "length" : 5,
                  "offset" : 11
               }
            ],
            "secondary_text" : "Long Island, Bedford Avenue, Brooklyn, NY, United States",
            "secondary_text_matched_substrings" : [
               {
                  "length" : 3,
                  "offset" : 0
               }
            ]
         },
         "terms" : [
            {
               "offset" : 0,
               "value" : "Anna Maria Pizza"
            },
            {
               "offset" : 18,
               "value" : "Long Island"
            },
            {
               "offset" : 31,
               "value" : "Bedford Avenue"
            },
            {
               "offset" : 47,
               "value" : "Brooklyn"
            },
            {
               "offset" : 57,
               "value" : "NY"
            },
            {
               "offset" : 61,
               "value" : "United States"
            }
         ],
         "types" : [ "establishment" ]
      }
   ],
   "status" : "OK"
}';
        $builder = new BuildResultGooglePlaceAutoComplete($str);
        $obj = $builder->get();
        $this->assertNotEmpty($obj);
        $this->assertEquals(5, $builder->count());
        $this->assertArrayHasKey('id', $obj[0]);
        $this->assertArrayHasKey('place_id', $obj[0]);
        $this->assertArrayHasKey('reference', $obj[0]);
        $this->assertArrayHasKey('description', $obj[0]);
    }

    public function testCallApiAutoCompleteSearchPlace()
    {
        $res = $this->json('POST', 'autocomplete_search_place', [
            'search' => [
                'keyword' => 'อาหาร'
            ],
        ]);
        $res->assertStatus(200)->assertJson([
            'success' => true,
        ]);
    }

    public function testGetPlaceDetailFromGoogle()
    {
        $d =   \GoogleMaps::load('placedetails')
            ->setParamByKey('placeid', 'ChIJN1t_tDeuEmsRUsoyG83frY4')
            ->get();
        $this->assertNotEmpty($d);
        $res = json_decode($d);
        $this->assertArrayHasKey('html_attributions', $res);
        $this->assertArrayHasKey('result', $res);
        $this->assertArrayHasKey('OK', $res);
    }

    public function testBuildResultPlaceDetail()
    {
        $res = '{
   "html_attributions" : [],
   "result" : {
      "address_components" : [
         {
            "long_name" : "5",
            "short_name" : "5",
            "types" : [ "floor" ]
         },
         {
            "long_name" : "48",
            "short_name" : "48",
            "types" : [ "street_number" ]
         },
         {
            "long_name" : "Pirrama Road",
            "short_name" : "Pirrama Rd",
            "types" : [ "route" ]
         },
         {
            "long_name" : "Pyrmont",
            "short_name" : "Pyrmont",
            "types" : [ "locality", "political" ]
         },
         {
            "long_name" : "Council of the City of Sydney",
            "short_name" : "Sydney",
            "types" : [ "administrative_area_level_2", "political" ]
         },
         {
            "long_name" : "New South Wales",
            "short_name" : "NSW",
            "types" : [ "administrative_area_level_1", "political" ]
         },
         {
            "long_name" : "Australia",
            "short_name" : "AU",
            "types" : [ "country", "political" ]
         },
         {
            "long_name" : "2009",
            "short_name" : "2009",
            "types" : [ "postal_code" ]
         }
      ],
      "adr_address" : "5, \u003cspan class=\"street-address\"\u003e48 Pirrama Rd\u003c/span\u003e, \u003cspan class=\"locality\"\u003ePyrmont\u003c/span\u003e \u003cspan class=\"region\"\u003eNSW\u003c/span\u003e \u003cspan class=\"postal-code\"\u003e2009\u003c/span\u003e, \u003cspan class=\"country-name\"\u003eAustralia\u003c/span\u003e",
      "formatted_address" : "5, 48 Pirrama Rd, Pyrmont NSW 2009, Australia",
      "formatted_phone_number" : "(02) 9374 4000",
      "geometry" : {
         "location" : {
            "lat" : -33.866651,
            "lng" : 151.195827
         },
         "viewport" : {
            "northeast" : {
               "lat" : -33.8653881697085,
               "lng" : 151.1969739802915
            },
            "southwest" : {
               "lat" : -33.86808613029149,
               "lng" : 151.1942760197085
            }
         }
      },
      "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
      "id" : "4f89212bf76dde31f092cfc14d7506555d85b5c7",
      "international_phone_number" : "+61 2 9374 4000",
      "name" : "Google",
      "opening_hours" : {
         "open_now" : false,
         "periods" : [
            {
               "close" : {
                  "day" : 1,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 1,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 2,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 2,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 3,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 3,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 4,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 4,
                  "time" : "1000"
               }
            },
            {
               "close" : {
                  "day" : 5,
                  "time" : "1800"
               },
               "open" : {
                  "day" : 5,
                  "time" : "1000"
               }
            }
         ],
         "weekday_text" : [
            "Monday: 10:00 AM – 6:00 PM",
            "Tuesday: 10:00 AM – 6:00 PM",
            "Wednesday: 10:00 AM – 6:00 PM",
            "Thursday: 10:00 AM – 6:00 PM",
            "Friday: 10:00 AM – 6:00 PM",
            "Saturday: Closed",
            "Sunday: Closed"
         ]
      },
      "photos" : [
         {
            "height" : 1365,
            "html_attributions" : [
               "\u003ca href=\"https://maps.google.com/maps/contrib/105932078588305868215/photos\"\u003eMaksym Kozlenko\u003c/a\u003e"
            ],
            "photo_reference" : "CmRaAAAA_3x5BsXnPRo52r3nj05mypani-9hmYvs630ozyNNFmEfVs71qQgo9EEie5QFBLmn1YflG3wM-9uQDEg2nKXhruDMdBpFNLsfRXhroi3ewtTKQH5Rp2-RNP_gANLsFnAkEhDT2KAzk66SubjYcaZ5LH6iGhSv473bOg5kwbGwM_H2qmYyVV0eQQ",
            "width" : 2048
         },
         {
            "height" : 900,
            "html_attributions" : [
               "\u003ca href=\"https://maps.google.com/maps/contrib/114853289796780923190/photos\"\u003eShir Yehoshua\u003c/a\u003e"
            ],
            "photo_reference" : "CmRaAAAAVsK9DN7VLfqbvoK4Y1obKOBT0dlvWoDdGjWtRGe3DdVE5CQTwrJ8LBmm8ZgX4N_Aw-qjjn3KPrqDoc2Ki3o0Io8OK_Iu2Vu0_6xSOo7pnqdY2N3joWR-zNWDMroSTTwtEhA6Xs3S7D47duOpHyvdwD5BGhTdJPs0R1Ic2wONZmBymIhvktWgrA",
            "width" : 1600
         },
         {
            "height" : 3264,
            "html_attributions" : [
               "\u003ca href=\"https://maps.google.com/maps/contrib/102493344958625549078/photos\"\u003eThomas Li\u003c/a\u003e"
            ],
            "photo_reference" : "CmRaAAAA3F5Sh6_V4kiaHFFkuAoJsv9p02c0KShjUC3STXrglBgnadR9UJDWHSD3fa_2IfNN4Pw2ODZ6nhQ7LOsu4vb9fXCCcqYJoL8L-LwmRUCAnBDgXi9mH7bJSdRXgVBDH3fLEhDBys4QpIHbOe4FXp2wb7WrGhRr6Fuvq5-W46G2kKSnxVmgDuZ4nQ",
            "width" : 4912
         },
         {
            "height" : 3024,
            "html_attributions" : [
               "\u003ca href=\"https://maps.google.com/maps/contrib/115886271727815775491/photos\"\u003eAnthony Huynh\u003c/a\u003e"
            ],
            "photo_reference" : "CmRaAAAABhwsWXGQZ_unAXEeN6wniJ435Z1aCnWe7n1rAwDbPeriS27iSf_CfXVkQwX9RZsXXJXbTV7F_worGA13NXl8YEfxPbuyLw8nMd_2Q2sOqFgK-sEuF54yb24ITDoyIH3aEhDC4udvq73-vqefWaizNxnNGhTi0YAMdFMjiPBJKQwHp4tGNE6PaA",
            "width" : 4032
         },
         {
            "height" : 1184,
            "html_attributions" : [
               "\u003ca href=\"https://maps.google.com/maps/contrib/106645265231048995466/photos\"\u003eMalik Ahamed\u003c/a\u003e"
            ],
            "photo_reference" : "CmRaAAAAJPu0Z9WbBsVH4kXrvmtoGvuKpJPz1vC2mYsOhkWKMPmbrqE_G2NXn4mZz0whQs2pPvlcO77prYUjk-z1DLgW_EjTpJUHYyRxNnMVfjfETed4wNJ5kqa9O8tG965W6M8dEhAxErDivxDPxZ5LLB6yPIaRGhR1Fc3qhYlaCpxPu0YPHaedRz7MLQ",
            "width" : 1776
         },
         {
            "height" : 5582,
            "html_attributions" : [
               "\u003ca href=\"https://maps.google.com/maps/contrib/110754641211532656340/photos\"\u003eRobert Koch\u003c/a\u003e"
            ],
            "photo_reference" : "CmRaAAAARdvRZNMe1k2-huo_oC61ljaOmIsHyF2X8NMDG7IizVq78UdtfJ1voo1cmlK-8TjnZwFDwO_Xa5mZs9bEI6IccM1IupVHq5Q5RN7mzF5Qpf7n5Ng9wiOZu0UifuxaTToqEhDA2e57tA0GI3DHdUe1QmqIGhSoaMWNxtrydNZ-7-1Su-aHy92U5g",
            "width" : 2866
         },
         {
            "height" : 4032,
            "html_attributions" : [
               "\u003ca href=\"https://maps.google.com/maps/contrib/102558609090086310801/photos\"\u003eHuy Tran\u003c/a\u003e"
            ],
            "photo_reference" : "CmRaAAAAVf3XPWvW2IN6o2UKRzWkH8BUM0EH8s8_3caTqhfBz7xiWaa3v0RJU9HbWyq9B2AF6kY_LnHn7oQgTVo40_mtwlh6B_-n2o7wPkU0_nCF9qEh7WZXeJzWNM4xsvQXJXhlEhAasaL4Pr0AArFS774T12wUGhQOEonkWut-Nw63ftzvi2tuw_nxQw",
            "width" : 3024
         },
         {
            "height" : 1944,
            "html_attributions" : [
               "\u003ca href=\"https://maps.google.com/maps/contrib/115237891004485589752/photos\"\u003eKatherine Howell\u003c/a\u003e"
            ],
            "photo_reference" : "CmRaAAAA2jZqD3C1Z_YjTogIwZ-tkCKqdqJe5eaQdyNwpMzXo1L5BCoEfwi_xH4vNm2_Xn92itt5ge2Py_QtGZ9xLIsO9WXImUIyliRvrQ3Basghx_vSYKGO1O_5BfHgaei0xS_gEhDgHt7Zeh8O0USG2WmjFWl-GhS2MyWSJOzdA2k7vEuCnLJzK_t54A",
            "width" : 1944
         },
         {
            "height" : 2448,
            "html_attributions" : [
               "\u003ca href=\"https://maps.google.com/maps/contrib/116976377324210679577/photos\"\u003eWH CHEN\u003c/a\u003e"
            ],
            "photo_reference" : "CmRaAAAAxQGfS3jDJIAOms2CsUQzU5_AywNLm2taRIHgh_RsVTKNlau0sH74fkeK-RoMLWLRPktSd3ukD4GGJgUnMrYXqhGmJ_tgFgLp7buw7ROKEz2Jl5hV13rrRZXAEbVR6ibdEhADjLW8TC4in10I9tgnc2WqGhQHkqMf4nzYu89n5F5CZ6mUV0re6A",
            "width" : 3264
         },
         {
            "height" : 2988,
            "html_attributions" : [
               "\u003ca href=\"https://maps.google.com/maps/contrib/109246940950895122662/photos\"\u003eBen Tubridy\u003c/a\u003e"
            ],
            "photo_reference" : "CmRaAAAA8iviLCoRruRcBJFIIUWFt_PGzzaLXCUi4SNGrJDy66kLqacUb2Zo4cNQTheg--qjlv6v4rE_raF1eMQDxLsdtJ_TwzE-HrpVYAe-R-9mRe7DUoFt4cpL6BnN_IgvTKnvEhDQpmXjyZE_UbDOhPb6DOKRGhRrrDb1ag9bctSkX54nJBjWngBJzg",
            "width" : 5312
         }
      ],
      "place_id" : "ChIJN1t_tDeuEmsRUsoyG83frY4",
      "rating" : 4.5,
      "reference" : "CmRSAAAAJf0y5ZBsAikJ_QDeRVsYxT4R0qly0YSIJi2Ww9XPZxw2a6xxDY_MY45F5X12x2JdKtmUKKkcop0iFl2H1COW0lmHnS3g4A8bKnvB87Bp3wThHAhz4TNoi5bsckj7WHeiEhAdmYUWQTj96ds5JEfPugM2GhT-6kopbuMmjNiotBlup5csFzwROQ",
      "reviews" : [
         {
            "author_name" : "Mark Sales",
            "author_url" : "https://www.google.com/maps/contrib/100341567599258416785/reviews",
            "language" : "en",
            "profile_photo_url" : "https://lh6.googleusercontent.com/-e2bgb-ognDY/AAAAAAAAAAI/AAAAAAAAAAA/AAyYBF5K8QcyGb-B5_yoiWjlWNTXqBcLHA/s128-c0x00000000-cc-rp-mo/photo.jpg",
            "rating" : 1,
            "relative_time_description" : "a week ago",
            "text" : "You have to  FIX your Google MY BUSINESS page business.\nWhy the business owner CANNOT delete photos uploaded by anyone.\nI can delete the photos I uploaded but why is it so strange that anyone can upload and the owner of that page cannot delete the photos. I called the call centre and I have to explain IN DETAIL why i want to delete a photo!!!\nThats a total JOKE!!! its my page so i can do whatever I want!!!! So if the photo that a customer is legit and it was 10 years ago, i CANNOT delete that??? USELESS and TOTAL WASTE...",
            "time" : 1496880715
         },
         {
            "author_name" : "Starland Painting Pty Ltd",
            "author_url" : "https://www.google.com/maps/contrib/106844006614491278928/reviews",
            "language" : "en",
            "profile_photo_url" : "https://lh4.googleusercontent.com/-zHEV7zQnWfM/AAAAAAAAAAI/AAAAAAAAAAk/aZukMkHPI_o/s128-c0x00000000-cc-rp-mo/photo.jpg",
            "rating" : 1,
            "relative_time_description" : "in the last week",
            "text" : "No customer support, indeed! \nI went through the most of the reviews provided by people whom somehow do business with Google, and I have found that I am not the only disappointed one!!\nMost, almost all, + ve comments are about Google\'s geo-location and their food..., presumably written by their employees... . \n\nGoogle is a massive cooperation that has the ability/power to do better, indeed, but why they don\'t, remains unknown, at least to me.\n\nWithout the presence of small or large scale businesses, Google is absolutely nothing but a Website! The foreseeable future may not be as good as it is now for Google, as some fresh competitors may make Google a history, considering the fact that Nothing Is Impossible!",
            "time" : 1496982169
         },
         {
            "author_name" : "Paul Sutherland",
            "author_url" : "https://www.google.com/maps/contrib/104671394445218170123/reviews",
            "language" : "en",
            "profile_photo_url" : "https://lh6.googleusercontent.com/-ZRFv8AHxqEQ/AAAAAAAAAAI/AAAAAAABsfg/HluyrsFH2bk/s128-c0x00000000-cc-rp-mo/photo.jpg",
            "rating" : 1,
            "relative_time_description" : "a month ago",
            "text" : "Very disappointed. I have been a supporter of google and some of its innovations, I particular love android and adwords helps my business. Now I have a review on my site that is in breach of defamation laws, however Google don\'t seem to care. This is a fake review by someone who doesn\'t identify themselves. I get that reviews are there to assist businesses better themselves and help consumers make a decision, but when its defamatory and hurtful the line needs to be better managed by google. Having read most of your one star reviews, most of them have the same issue. Time to listen to your customers Google.",
            "time" : 1493618915
         },
         {
            "author_name" : "Liam Gartzos",
            "author_url" : "https://www.google.com/maps/contrib/117200884204662291544/reviews",
            "language" : "en",
            "profile_photo_url" : "https://lh6.googleusercontent.com/-bFgppkBdLj8/AAAAAAAAAAI/AAAAAAAAADU/ebOl2WUS49M/s128-c0x00000000-cc-rp-mo/photo.jpg",
            "rating" : 5,
            "relative_time_description" : "2 weeks ago",
            "text" : "This facility has many unorthodox problems and we can all be yay, no this was a great place to unfold on google however i would be inclined to ask why 3% of the internet is google and other web services yet chrome or......internet exploror",
            "time" : 1496029863
         },
         {
            "author_name" : "Ranjit Nair",
            "author_url" : "https://www.google.com/maps/contrib/102808647017735332248/reviews",
            "language" : "en",
            "profile_photo_url" : "https://lh4.googleusercontent.com/-A-UJAO1hMtk/AAAAAAAAAAI/AAAAAAAAAAA/AAyYBF5ojcf5EM9vU3KsroX_DEyKMn76MA/s128-c0x00000000-cc-rp-mo/photo.jpg",
            "rating" : 1,
            "relative_time_description" : "4 months ago",
            "text" : "Absolutely ZERO support. There is no thought behind how the whole Google Business Review process works and the only answer you get from support staff is that they are only trained to direct people to the Google support page. Why do you need people at the end of a phone line to tell you that?\n\nAs it stands, there is nothing stopping me from standing in front of a store and giving a negative review to a business based on whether it was raining that day or not. There is no process here to verify if the claim of the user is right or not. I thought the whole process of reviews were meant to be a fair representation of the service the business provides. Where is the fairness here?\n\nGoogle encourages businesses to respond to people\'s reviews. Is Google responding to the reviews that are posted about their business? Why the double standards Google?",
            "time" : 1485900942
         }
      ],
      "scope" : "GOOGLE",
      "types" : [ "point_of_interest", "establishment" ],
      "url" : "https://maps.google.com/?cid=10281119596374313554",
      "utc_offset" : 600,
      "vicinity" : "5, 48 Pirrama Road, Pyrmont",
      "website" : "https://www.google.com.au/about/careers/locations/sydney/"
   },
   "status" : "OK"
}
';
        $builder = new BuildResultGooglePlaceDetail($res);
        $res = $builder->get();
        $this->assertArrayHasKey('id', $res);
        $this->assertArrayHasKey('name', $res);
    }
}