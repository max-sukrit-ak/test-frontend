@extends('layout.layout')

@section('content')
    <nav class="test-frontend-nav navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"><b>CLBS Test Frontend</b></a>
            </div>
        </div>
    </nav>
    <div ui-view class="content-view"></div>
@endsection

@push('scripts')
<script src="{{ asset('angular/home/index.js') }}"></script>
<script src="{{ asset('angular/routes.js') }}"></script>
<script src="{{ asset('angular/directives.js') }}"></script>
<script src="{{ asset('angular/api.js') }}"></script>
<script src="{{ asset('angular/app.js') }}"></script>
@endpush