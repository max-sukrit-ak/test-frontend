<link rel="stylesheet" href="{{asset('css/app.css')}}"/>

<link rel="stylesheet" href="{{asset('components/font-awesome/css/font-awesome.min.css')}}"/>

<link rel="stylesheet" href="{{asset('components/ng-table/dist/ng-table.min.css')}}"/>
<link rel="stylesheet" href="{{asset('components/angular-ui-notification/dist/angular-ui-notification.min.css')}}"/>
<link rel="stylesheet" href="{{asset('components/angucomplete/angucomplete.css')}}"/>
<link rel="stylesheet" href="{{asset('components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}"/>
<link rel="stylesheet" href="{{asset('components/font-awesome/css/font-awesome.min.css')}}"/>
<link rel="stylesheet" href="{{asset('components/angular-bootstrap/ui-bootstrap-csp.css')}}"/>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

@stack('stylesheet')