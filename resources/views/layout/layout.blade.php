<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Test - Frontend</title>

    @include('layout.stylesheet')
</head>
<body ng-app="test_frontend">

@yield('content')

@include('layout.javascript')
</body>
</html>