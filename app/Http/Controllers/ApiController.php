<?php
/**
 * Created by PhpStorm.
 * User: MAX
 * Date: 10/6/2560
 * Time: 13:38
 */

namespace App\Http\Controllers;


use App\Http\Library\BuildResultGooglePlace;
use App\Http\Library\BuildResultGooglePlaceAutoComplete;
use App\Http\Library\BuildResultGooglePlaceDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    public function search_place(Request $request)
    {
        try {
            $params = $request->get('search', []);
            $rules = [
                'lat' => 'required',
                'lng' => 'required',
                'keyword' => 'max:100',
                'radius' => 'required|max:50000',
            ];
            $validator = Validator::make($params, $rules);
            if ($validator->fails()) {
                return Response()->json([
                    "errors" => $validator->errors(),
                    "message" => "Invalid data."
                ]);
            }

            $keyCache = "{$params['lat']}_{$params['lng']}_{$params['radius']}_{$params['keyword']}";

            $res = Cache::remember($keyCache, 60 * 24, function () use ($params) {
                $res = \GoogleMaps::load('nearbysearch')
                    ->setParam([
                        'location' => "{$params['lat']},{$params['lng']}",
                        'radius' => $params['radius'],
                        'keyword' => $params['keyword'],
                    ])
                    ->get();
                return $res;
            });
            $builder = new BuildResultGooglePlace($res);
            return Response()->json([
                'success' => true,
                'data' => $builder->get($params['lat'], $params['lng'])
            ], 200);
        } catch (\Exception $e) {
            return Response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 404);
        }
    }

    public function autocomplete_search_place(Request $request)
    {
        try {
            $params = $request->all();
            $rules = [
                'keyword' => 'required|max:100',
            ];
            $validator = Validator::make($params, $rules);
            if ($validator->fails()) {
                return Response()->json([
                    "errors" => $validator->errors(),
                    "message" => "Invalid data."
                ]);
            }

            $keyCache = "{$params['keyword']}";

            $res = Cache::remember($keyCache, 60 * 24, function () use ($params) {
                $res = \GoogleMaps::load('placequeryautocomplete')
                    ->setParamByKey('input', $params['keyword'])
                    ->setParamByKey('language', 'th')
                    ->get();
                return $res;
            });
            $builder = new BuildResultGooglePlaceAutoComplete($res);
            return Response()->json([
                'success' => true,
                'data' => $builder->get()
            ], 200);
        } catch (\Exception $e) {
            return Response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 404);
        }
    }

    public function photo_place(Request $request)
    {
        try {
            $params = $request->all();
            $rules = [
                'maxwidth' => 'required',
                'photoreference' => 'required',
            ];
            $validator = Validator::make($params, $rules);
            if ($validator->fails()) {
                return Response()->json([
                    "errors" => $validator->errors(),
                    "message" => "Invalid data."
                ]);
            }
            $keyCache = "{$params['maxwidth']}_{$params['photoreference']}";
            $res = Cache::remember($keyCache, 60 * 24, function () use ($params) {
                return \GoogleMaps::load('placephoto')
                    ->setParamByKey('maxwidth', $params['maxwidth'])
                    ->setParamByKey('photoreference', $params['photoreference'])
                    ->get();
            });
            return \Intervention\Image\Facades\Image::make($res)->response();
        } catch (\Exception $e) {
            return Response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 404);
        }
    }

    public function place_detail(Request $request)
    {
        try {
            $params = $request->all();
            $rules = [
                'place_id' => 'required',
            ];
            $validator = Validator::make($params, $rules);
            if ($validator->fails()) {
                return Response()->json([
                    "errors" => $validator->errors(),
                    "message" => "Invalid data."
                ]);
            }

            $keyCache = "place_detail_{$params['place_id']}";

            $res = Cache::remember($keyCache, 60 * 24, function () use ($params) {
                $res = \GoogleMaps::load('placedetails')
                    ->setParamByKey('placeid', $params['place_id'])
                    ->get();
                return $res;
            });
            $builder = new BuildResultGooglePlaceDetail($res);
            return Response()->json([
                'success' => true,
                'data' => $builder->get()
            ], 200);
        } catch (\Exception $e) {
            return Response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 404);
        }
    }
}