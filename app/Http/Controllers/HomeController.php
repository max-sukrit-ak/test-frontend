<?php
/**
 * Created by PhpStorm.
 * User: MAX
 * Date: 10/6/2560
 * Time: 12:05
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        return view('index');
    }
}