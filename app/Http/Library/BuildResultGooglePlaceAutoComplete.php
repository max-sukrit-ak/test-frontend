<?php

/**
 * Created by PhpStorm.
 * User: MAX
 * Date: 10/6/2560
 * Time: 22:07
 */

namespace App\Http\Library;

class BuildResultGooglePlaceAutoComplete
{
    private $_res = null;
    private $_obj = [];
    function __construct($res)
    {
        $this->makeToArray($res);
    }

    private function makeToArray($res)
    {
        $val = null;
        switch (gettype($res)) {
            case 'string':
                $val = json_decode($res, true);
                break;
            case 'array':
                $val = $res;
                break;
        }
        $this->_res = $val;
    }

    public function count()
    {
        return count($this->_obj);
    }

    public function get()
    {
        $obj = [];
        if ($this->_res) {
            if (array_key_exists('predictions', $this->_res) && count($this->_res['predictions']) > 0) {
                foreach ($this->_res['predictions'] as $result) {
                    $o = [];

                    if (array_key_exists('id', $result)) {
                        $o['id'] = $result['id'];
                    }
                    if (array_key_exists('place_id', $result)) {
                        $o['place_id'] = $result['place_id'];
                    }
                    if (array_key_exists('reference', $result)) {
//                        $key = 'AIzaSyAmp7wfaKmrCr15Y9CUI1I9HuxH3kGrZd4';
                        $o['reference'] = $result['reference'];
//                        $o['image_reference'] = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&maxheight=300&key={$key}&photoreference={$o['reference']}";
                        $o['image_reference'] = url("/photo_place?maxwidth=300&maxheight=300&photoreference={$o['reference']}");
                    }
                    if (array_key_exists('description', $result)) {
                        $o['description'] = $result['description'];
                    }

                    if ($o && count($o) > 0) {
                        $obj[] = $o;
                    }
                }
            }
        }
        $this->_obj = $obj;
        return $this->_obj;
    }
}