<?php

/**
 * Created by PhpStorm.
 * User: MAX
 * Date: 10/6/2560
 * Time: 22:07
 */

namespace App\Http\Library;

class BuildResultGooglePlace
{
    private $_res = null;
    private $_obj = [];
    function __construct($res)
    {
        $this->makeToArray($res);
    }

    private function makeToArray($res)
    {
        $val = null;
        switch (gettype($res)) {
            case 'string':
                $val = json_decode($res, true);
                break;
            case 'array':
                $val = $res;
                break;
        }
        $this->_res = $val;
    }

    public function count()
    {
        return count($this->_obj);
    }

    private function distance($lat1, $lng1, $lat2, $lng2)
    {
        $x = 111.12 * ($lat2 - $lat1);
        $y = 111.12 * ($lng2 - $lng1) * cos($lat1 / 92.215);
        return sqrt($x * $x + $y * $y);
    }

    public function get($lat, $lng)
    {
        $obj = [];
        if ($this->_res) {
            if (array_key_exists('results', $this->_res) && count($this->_res['results']) > 0) {
                foreach ($this->_res['results'] as $result) {
                    $o = [];

                    if (array_key_exists('id', $result)) {
                        $o['id'] = $result['id'];
                    }
                    if (array_key_exists('place_id', $result)) {
                        $o['place_id'] = $result['place_id'];
                    }
                    if (array_key_exists('name', $result)) {
                        $o['name'] = $result['name'];
                    }
                    if (array_key_exists('vicinity', $result)) {
                        $o['vicinity'] = $result['vicinity'];
                    }
                    if (array_key_exists('types', $result) && count($result['types']) > 0) {
                        $o['types'] = $result['types'][0];
                    }
                    if (array_key_exists('rating', $result)) {
                        $o['rating'] = $result['rating'];
                    }
                    if (array_key_exists('photos', $result) && count($result['photos']) > 0) {
                        $photo = $result['photos'][0];
                        if (array_key_exists('photo_reference', $photo)) {
//                            $key = 'AIzaSyAmp7wfaKmrCr15Y9CUI1I9HuxH3kGrZd4';
//                            $o['url_image'] = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&maxheight=300&key={$key}&photoreference={$photo['photo_reference']}";
                            $o['url_image'] = url("/photo_place?maxwidth=300&maxheight=300&photoreference={$photo['photo_reference']}");
                        }
                    }
                    if (array_key_exists('opening_hours', $result)) {
                        $opening_hours = $result['opening_hours'];
                        if (array_key_exists('open_now', $opening_hours)) {
                            $o['open_now'] = $opening_hours['open_now'];
                        }
                    }
                    if (array_key_exists('geometry', $result)) {
                        $geometry = $result['geometry'];
                        if (array_key_exists('location', $geometry)) {
                            $location = $geometry['location'];
                            if (array_key_exists('lat', $location)) {
                                $o['lat'] = $location['lat'];
                            }
                            if (array_key_exists('lng', $location)) {
                                $o['lng'] = $location['lng'];
                            }
                        }
                    }

                    $o['distance'] = null;
                    if (array_key_exists('lat', $o) && array_key_exists('lng', $o)) {
                        $d = $this->distance($lat, $lng, $o['lat'], $o['lng']);
                        $o['distance'] = number_format($d, 2, '.', '');
                    }

                    if ($o && count($o) > 0 && array_key_exists('lat', $o) && array_key_exists('lng', $o)) {
                        $obj[] = $o;
                    }
                }
            }
        }
        $this->_obj = $obj;
        return $this->_obj;
    }
}