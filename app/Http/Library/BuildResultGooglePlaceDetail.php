<?php

/**
 * Created by PhpStorm.
 * User: MAX
 * Date: 15/6/2560
 * Time: 23:29
 */

namespace App\Http\Library;

class BuildResultGooglePlaceDetail
{
    private $_res = null;
    private $_obj = [];
    function __construct($res)
    {
        $this->makeToArray($res);
    }

    private function makeToArray($res)
    {
        $val = null;
        switch (gettype($res)) {
            case 'string':
                $val = json_decode($res, true);
                break;
            case 'array':
                $val = $res;
                break;
        }
        $this->_res = $val;
    }

    private function distance($lat1, $lng1, $lat2, $lng2)
    {
        $x = 111.12 * ($lat2 - $lat1);
        $y = 111.12 * ($lng2 - $lng1) * cos($lat1 / 92.215);
        return sqrt($x * $x + $y * $y);
    }

    public function get()
    {
        $obj = [];
        if ($this->_res) {
            if (array_key_exists('result', $this->_res)) {
                $result = $this->_res['result'];
                if (array_key_exists('id', $result)) {
                    $obj['id'] = $result['id'];
                }
                if (array_key_exists('name', $result)) {
                    $obj['name'] = $result['name'];
                }
                if (array_key_exists('formatted_address', $result)) {
                    $obj['address'] = $result['formatted_address'];
                }
                if (array_key_exists('vicinity', $result)) {
                    $o['vicinity'] = $result['vicinity'];
                }
                if (array_key_exists('photos', $result)) {
                    $photos = $result['photos'];
                    if (isset($photos) && count($photos) > 0) {
                        $obj['photos'] = [];
                        foreach ($photos as $photo) {
                            $obj['photos'][] = url("/photo_place?maxwidth=300&maxheight=300&photoreference={$photo['photo_reference']}");
                        }
                    }
                }
                if (array_key_exists('reviews', $result)) {
                    $reviews = $result['reviews'];
                    if (isset($reviews) && count($reviews) > 0) {
                        $obj['reviews'] = [];
                        foreach ($reviews as $review) {
                            $obj['reviews'][] = $review;
                        }
                    }
                }
            }
        }
        $this->_obj = $obj;
        return $this->_obj;
    }
}